# Maintainer: Philip Müller <philm[AT]manjaro[DOT]org>
# Maintainer: Helmut Stult <helmut[AT]manjaro[DOT]org>

# Based on the file created for TUXEDO Computers by:
# Maintainer: Steven Seifried <gitlab@canox.net>

pkgname=tuxedo-control-center
pkgver=1.1.0
pkgrel=1
pkgdesc="A tool to help you control performance, energy, fan and comfort settings on TUXEDO laptops."
arch=(x86_64)
url="https://github.com/tuxedocomputers/tuxedo-control-center"
license=('GPL3')
depends=('tuxedo-keyboard' 'libxss' 'nss' 'gtk3')
options=(!strip)
install=install
source=("https://rpm.tuxedocomputers.com/opensuse/15.2/x86_64/${pkgname}_${pkgver}.rpm"
        "${pkgname}.desktop")
sha256sums=('0c20d71ba73d5320b7487193096be0647953bbf5b8db1dec9c005aaa43741067'
            '2e588bd6124c60896b1bef47cfd81ee2fc53129effbe6a2f822c7e4c139fbf73')

package() {
  cp -av usr "${pkgdir}"
  cp -av opt "${pkgdir}"
  chmod -R 755 "${pkgdir}"/opt/${pkgname}/
  mkdir -p "${pkgdir}/usr/bin"
  ln -sfv /opt/${pkgname}/${pkgname} "${pkgdir}/usr/bin/tuxedo-control-center"
  install -Dm755 ${pkgname}.desktop "${pkgdir}/usr/share/applications"
  install -Dm644 "${srcdir}/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"
  install -Dm644 "${srcdir}/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data/de.tuxedocomputers.tcc.policy" "${pkgdir}/usr/share/polkit-1/actions/de.tuxedocomputers.tcc.policy"
  install -Dm644 "${srcdir}/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data/com.tuxedocomputers.tccd.conf" "${pkgdir}/usr/share/dbus-1/system.d/com.tuxedocomputers.tccd.conf"
  install -Dm644 "$srcdir/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data/tccd.service" "${pkgdir}/etc/systemd/system/tccd.service"
  install -Dm644 "$srcdir/opt/${pkgname}/resources/dist/${pkgname}/data/dist-data/tccd-sleep.service" "${pkgdir}/etc/systemd/system/tccd-sleep.service"
}
